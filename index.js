const github = require('octonode');
const request = require("request");
const btoa = require("btoa-lite");
const Bitbucket = require("bitbucket");
const bitbucket = new Bitbucket();
const fs = require("fs-extra");
if (!fs.existsSync('./git_tmp')) {
    fs.mkdirSync('./git_tmp');
}
else {
    fs.removeSync('./git_tmp');
    fs.mkdirSync('./git_tmp');
}

const git = require('simple-git/promise')('./git_tmp');


const githubUsername = "";
const githubPassword = "";

const bitbucketUsername = "";
const bitbucketPassword = "2"

bitbucket.authenticate({
    type: 'basic',
    username: bitbucketUsername,
    password: bitbucketPassword
});

var client = github.client({
    username: githubUsername,
    password: githubPassword
});

var ghme = client.me();

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array)
    }
}

async function cloneRepo(name, newOrigin) {
    const REPO = 'github.com/' + githubUsername + '/' + name;


    const remote = `https://${githubUsername}:${githubPassword}@${REPO}`;

    return new Promise((resolve, reject) => {
        git.silent(true)
            .clone(remote, '.')
            .then(() => {
                git.removeRemote("origin").then(() => {
                    git.addRemote('origin', newOrigin).then(() => {
                        git.fetch().then(() => {
                            git.push(['-u', 'origin', 'master']).then(() => {
                                resolve(true)
                            });
                        })
                    })
                });

            })
            .catch((err) => resolve(false));
    })

}

async function clearRepo() {
    return new Promise((resolve, reject) => {
        fs.removeSync('./git_tmp')
        fs.mkdirSync('./git_tmp')
        resolve();
    })
}

async function clearBitbucket() {
    bitbucket.repositories.list({username: bitbucketUsername, pagelen: 50}).then(({data, headers}) => {
        data.values.forEach(repo => {
            bitbucket.repositories.delete({
                "repo_slug": repo.slug,
                "username": bitbucketUsername
            }).then(d => {
                console.log(d)
            })
        })
    })
}

async function createPrivateRepo(name) {

    return new Promise((resolve, reject) => {
        bitbucket.repositories
            .create({
                "_body": {is_private: true},
                "repo_slug": name,
                "username": bitbucketUsername
            }).then(data => {
            resolve(data)
        }).catch(e => {
            reject(e)
        })
    })
}

async function getPrivateRepos(page) {
    if (!page)
        page = 1;

    return new Promise((resolve, reject) => {
        ghme.repos({
            page: page,
            per_page: 200
        }, (e, t) => {
            if (e) {
                console.log(e)
            }
            let repos = t.filter(x => x.private === true && x.owner.login === githubUsername)
            resolve(repos)
        })
    })
}

(async () => {

    let repos = [];
    let page = 1;
    let repoPage = [];

    do {
        repoPage = await getPrivateRepos(page);
        repos = repos.concat(repoPage);
        page += 1;
    } while (repoPage.length > 0);

    // await clearBitbucket() //deletes all repos on bitbucket account

    asyncForEach(repos, async repo => {
        try {
            console.log("=====")
            console.log("Repo:", repo.full_name)
            let data = await createPrivateRepo(repo.name.toLowerCase().replace(/^(.(?![a-z_\-\.0-9]))*/g, ""))
            console.log("Bitbucket Repo Created.")
            console.log("Cloning...")
            let remote = 'git@bitbucket.org:' + bitbucketUsername + '/' + data.data.slug + '.git'
            await cloneRepo(repo.name, remote);
            console.log("Cloned and pushed:", remote)
            await clearRepo();
            //
            // console.log(data)
        } catch (e) {
            console.log(e)
        }
    })


})();
