# github2bitbucket

## Installation

`git clone git@bitbucket.org:wehmoen/github2bitbucket.git`

`npm install`

## Prepare

Open the index.js file and enter your github and bitbucket credentials and save the file.

## Usage

`node index.js`

### Notes
- Script throws an error when the source repository is empty.
- If the remote repo does already exist it is skipped.
- Bitbucket uses ssh to push the repo. Make sure that you add your local ssh key.
